﻿using System.Windows.Forms;

namespace DerbyTimer
{
    public partial class FormResults : Form
    {
        public FormResults()
        {
            InitializeComponent();
        }

        private void FormResults_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Hide();
            }
        }
    }
}
