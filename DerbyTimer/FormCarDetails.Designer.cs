﻿namespace DerbyTimer
{
    partial class FormCarDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCubName = new System.Windows.Forms.Label();
            this.CubName = new System.Windows.Forms.TextBox();
            this.lblCarName = new System.Windows.Forms.Label();
            this.CarName = new System.Windows.Forms.TextBox();
            this.weight = new System.Windows.Forms.NumericUpDown();
            this.lblWeight = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.section = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_another = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.weight)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCubName
            // 
            this.lblCubName.AutoSize = true;
            this.lblCubName.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCubName.Location = new System.Drawing.Point(18, 29);
            this.lblCubName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCubName.Name = "lblCubName";
            this.lblCubName.Size = new System.Drawing.Size(92, 18);
            this.lblCubName.TabIndex = 0;
            this.lblCubName.Text = "Cub Name";
            // 
            // CubName
            // 
            this.CubName.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CubName.Location = new System.Drawing.Point(109, 26);
            this.CubName.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.CubName.Name = "CubName";
            this.CubName.Size = new System.Drawing.Size(245, 27);
            this.CubName.TabIndex = 1;
            // 
            // lblCarName
            // 
            this.lblCarName.AutoSize = true;
            this.lblCarName.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCarName.Location = new System.Drawing.Point(18, 60);
            this.lblCarName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCarName.Name = "lblCarName";
            this.lblCarName.Size = new System.Drawing.Size(88, 18);
            this.lblCarName.TabIndex = 2;
            this.lblCarName.Text = "Car Name";
            // 
            // CarName
            // 
            this.CarName.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CarName.Location = new System.Drawing.Point(109, 58);
            this.CarName.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.CarName.Name = "CarName";
            this.CarName.Size = new System.Drawing.Size(245, 27);
            this.CarName.TabIndex = 2;
            // 
            // weight
            // 
            this.weight.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.weight.Location = new System.Drawing.Point(109, 97);
            this.weight.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.weight.Maximum = new decimal(new int[] {
            175,
            0,
            0,
            0});
            this.weight.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.weight.Name = "weight";
            this.weight.Size = new System.Drawing.Size(64, 27);
            this.weight.TabIndex = 3;
            this.weight.Value = new decimal(new int[] {
            142,
            0,
            0,
            0});
            // 
            // lblWeight
            // 
            this.lblWeight.AutoSize = true;
            this.lblWeight.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeight.Location = new System.Drawing.Point(18, 99);
            this.lblWeight.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(66, 18);
            this.lblWeight.TabIndex = 5;
            this.lblWeight.Text = "Weight";
            // 
            // btnSave
            // 
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(9, 152);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(59, 27);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(325, 152);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(66, 27);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnClear
            // 
            this.btnClear.DialogResult = System.Windows.Forms.DialogResult.Retry;
            this.btnClear.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Location = new System.Drawing.Point(257, 152);
            this.btnClear.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(59, 27);
            this.btnClear.TabIndex = 7;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // section
            // 
            this.section.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.section.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.section.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.section.FormattingEnabled = true;
            this.section.Items.AddRange(new object[] {
            "Joeys",
            "Cubs",
            "Scouts",
            "Venturers",
            "Open"});
            this.section.Location = new System.Drawing.Point(263, 97);
            this.section.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.section.Name = "section";
            this.section.Size = new System.Drawing.Size(92, 26);
            this.section.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(191, 99);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 18);
            this.label1.TabIndex = 11;
            this.label1.Text = "Section";
            // 
            // btn_another
            // 
            this.btn_another.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btn_another.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_another.Location = new System.Drawing.Point(77, 152);
            this.btn_another.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btn_another.Name = "btn_another";
            this.btn_another.Size = new System.Drawing.Size(171, 27);
            this.btn_another.TabIndex = 6;
            this.btn_another.Text = "Save + Another";
            this.btn_another.UseVisualStyleBackColor = true;
            this.btn_another.Click += new System.EventHandler(this.btn_another_Click);
            // 
            // FormCarDetails
            // 
            this.AcceptButton = this.btn_another;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(402, 189);
            this.Controls.Add(this.btn_another);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.section);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblWeight);
            this.Controls.Add(this.weight);
            this.Controls.Add(this.CarName);
            this.Controls.Add(this.lblCarName);
            this.Controls.Add(this.CubName);
            this.Controls.Add(this.lblCubName);
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "FormCarDetails";
            this.Text = "FormCarDetails";
            ((System.ComponentModel.ISupportInitialize)(this.weight)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCubName;
        private System.Windows.Forms.Label lblCarName;
        private System.Windows.Forms.Label lblWeight;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnClear;
        public System.Windows.Forms.TextBox CubName;
        public System.Windows.Forms.TextBox CarName;
        public System.Windows.Forms.NumericUpDown weight;
        public System.Windows.Forms.ComboBox section;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_another;
    }
}