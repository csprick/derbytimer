﻿namespace DerbyTimer
{
    partial class FormProjected
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblAtime = new System.Windows.Forms.Label();
            this.lblAplace = new System.Windows.Forms.Label();
            this.lblAcar = new System.Windows.Forms.Label();
            this.lblAcub = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblBtime = new System.Windows.Forms.Label();
            this.lblBplace = new System.Windows.Forms.Label();
            this.lblBcar = new System.Windows.Forms.Label();
            this.lblBcub = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblCtime = new System.Windows.Forms.Label();
            this.lblCplace = new System.Windows.Forms.Label();
            this.lblCcar = new System.Windows.Forms.Label();
            this.lblCcub = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblAnextCar = new System.Windows.Forms.Label();
            this.lblAnextCub = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblBnextCar = new System.Windows.Forms.Label();
            this.lblBnextCub = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lblCnextCar = new System.Windows.Forms.Label();
            this.lblCnextCub = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblDnextCar = new System.Windows.Forms.Label();
            this.lblDnextCub = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.lblDtime = new System.Windows.Forms.Label();
            this.lblDplace = new System.Windows.Forms.Label();
            this.lblDcar = new System.Windows.Forms.Label();
            this.lblDcub = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.panel1.Controls.Add(this.lblAtime);
            this.panel1.Controls.Add(this.lblAplace);
            this.panel1.Controls.Add(this.lblAcar);
            this.panel1.Controls.Add(this.lblAcub);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(14, 15);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(345, 395);
            this.panel1.TabIndex = 4;
            // 
            // lblAtime
            // 
            this.lblAtime.AutoSize = true;
            this.lblAtime.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtime.Location = new System.Drawing.Point(116, 332);
            this.lblAtime.Name = "lblAtime";
            this.lblAtime.Size = new System.Drawing.Size(97, 29);
            this.lblAtime.TabIndex = 4;
            this.lblAtime.Text = "0.0000";
            // 
            // lblAplace
            // 
            this.lblAplace.AutoSize = true;
            this.lblAplace.Font = new System.Drawing.Font("Verdana", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAplace.Location = new System.Drawing.Point(126, 228);
            this.lblAplace.Name = "lblAplace";
            this.lblAplace.Size = new System.Drawing.Size(60, 62);
            this.lblAplace.TabIndex = 3;
            this.lblAplace.Text = "1";
            // 
            // lblAcar
            // 
            this.lblAcar.AutoSize = true;
            this.lblAcar.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAcar.Location = new System.Drawing.Point(24, 144);
            this.lblAcar.Name = "lblAcar";
            this.lblAcar.Size = new System.Drawing.Size(99, 59);
            this.lblAcar.TabIndex = 2;
            this.lblAcar.Text = "car";
            // 
            // lblAcub
            // 
            this.lblAcub.AutoSize = true;
            this.lblAcub.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAcub.Location = new System.Drawing.Point(3, 68);
            this.lblAcub.Name = "lblAcub";
            this.lblAcub.Size = new System.Drawing.Size(299, 59);
            this.lblAcub.TabIndex = 1;
            this.lblAcub.Text = "racer name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(62, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(205, 48);
            this.label1.TabIndex = 0;
            this.label1.Text = "Red Lane";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.panel2.Controls.Add(this.lblBtime);
            this.panel2.Controls.Add(this.lblBplace);
            this.panel2.Controls.Add(this.lblBcar);
            this.panel2.Controls.Add(this.lblBcub);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Location = new System.Drawing.Point(365, 17);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(345, 394);
            this.panel2.TabIndex = 5;
            // 
            // lblBtime
            // 
            this.lblBtime.AutoSize = true;
            this.lblBtime.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBtime.Location = new System.Drawing.Point(99, 331);
            this.lblBtime.Name = "lblBtime";
            this.lblBtime.Size = new System.Drawing.Size(97, 29);
            this.lblBtime.TabIndex = 4;
            this.lblBtime.Text = "0.0000";
            // 
            // lblBplace
            // 
            this.lblBplace.AutoSize = true;
            this.lblBplace.Font = new System.Drawing.Font("Verdana", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBplace.Location = new System.Drawing.Point(124, 226);
            this.lblBplace.Name = "lblBplace";
            this.lblBplace.Size = new System.Drawing.Size(60, 62);
            this.lblBplace.TabIndex = 3;
            this.lblBplace.Text = "1";
            // 
            // lblBcar
            // 
            this.lblBcar.AutoSize = true;
            this.lblBcar.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBcar.Location = new System.Drawing.Point(26, 142);
            this.lblBcar.Name = "lblBcar";
            this.lblBcar.Size = new System.Drawing.Size(99, 59);
            this.lblBcar.TabIndex = 2;
            this.lblBcar.Text = "car";
            // 
            // lblBcub
            // 
            this.lblBcub.AutoSize = true;
            this.lblBcub.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBcub.Location = new System.Drawing.Point(0, 66);
            this.lblBcub.Name = "lblBcub";
            this.lblBcub.Size = new System.Drawing.Size(299, 59);
            this.lblBcub.TabIndex = 1;
            this.lblBcub.Text = "racer name";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(42, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(256, 48);
            this.label6.TabIndex = 0;
            this.label6.Text = "Yellow Lane";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.panel3.Controls.Add(this.lblCtime);
            this.panel3.Controls.Add(this.lblCplace);
            this.panel3.Controls.Add(this.lblCcar);
            this.panel3.Controls.Add(this.lblCcub);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Location = new System.Drawing.Point(716, 17);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(345, 394);
            this.panel3.TabIndex = 5;
            // 
            // lblCtime
            // 
            this.lblCtime.AutoSize = true;
            this.lblCtime.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCtime.Location = new System.Drawing.Point(123, 331);
            this.lblCtime.Name = "lblCtime";
            this.lblCtime.Size = new System.Drawing.Size(97, 29);
            this.lblCtime.TabIndex = 4;
            this.lblCtime.Text = "0.0000";
            // 
            // lblCplace
            // 
            this.lblCplace.AutoSize = true;
            this.lblCplace.Font = new System.Drawing.Font("Verdana", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCplace.Location = new System.Drawing.Point(134, 226);
            this.lblCplace.Name = "lblCplace";
            this.lblCplace.Size = new System.Drawing.Size(60, 62);
            this.lblCplace.TabIndex = 3;
            this.lblCplace.Text = "1";
            // 
            // lblCcar
            // 
            this.lblCcar.AutoSize = true;
            this.lblCcar.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCcar.Location = new System.Drawing.Point(33, 142);
            this.lblCcar.Name = "lblCcar";
            this.lblCcar.Size = new System.Drawing.Size(99, 59);
            this.lblCcar.TabIndex = 2;
            this.lblCcar.Text = "car";
            // 
            // lblCcub
            // 
            this.lblCcub.AutoSize = true;
            this.lblCcub.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCcub.Location = new System.Drawing.Point(3, 66);
            this.lblCcub.Name = "lblCcub";
            this.lblCcub.Size = new System.Drawing.Size(299, 59);
            this.lblCcub.TabIndex = 1;
            this.lblCcub.Text = "racer name";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Verdana", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(47, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(250, 48);
            this.label11.TabIndex = 0;
            this.label11.Text = "Green Lane";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(574, 442);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(182, 38);
            this.label2.TabIndex = 6;
            this.label2.Text = "Next Up...";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.lblAnextCar);
            this.panel5.Controls.Add(this.lblAnextCub);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel5.Location = new System.Drawing.Point(16, 505);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(345, 289);
            this.panel5.TabIndex = 7;
            // 
            // lblAnextCar
            // 
            this.lblAnextCar.AutoSize = true;
            this.lblAnextCar.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAnextCar.Location = new System.Drawing.Point(108, 197);
            this.lblAnextCar.Name = "lblAnextCar";
            this.lblAnextCar.Size = new System.Drawing.Size(86, 49);
            this.lblAnextCar.TabIndex = 2;
            this.lblAnextCar.Text = "car";
            // 
            // lblAnextCub
            // 
            this.lblAnextCub.AutoSize = true;
            this.lblAnextCub.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAnextCub.Location = new System.Drawing.Point(3, 102);
            this.lblAnextCub.Name = "lblAnextCub";
            this.lblAnextCub.Size = new System.Drawing.Size(256, 49);
            this.lblAnextCub.TabIndex = 1;
            this.lblAnextCub.Text = "racer name";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(74, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 49);
            this.label8.TabIndex = 0;
            this.label8.Text = "Red";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.lblBnextCar);
            this.panel6.Controls.Add(this.lblBnextCub);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel6.Location = new System.Drawing.Point(367, 505);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(345, 289);
            this.panel6.TabIndex = 8;
            // 
            // lblBnextCar
            // 
            this.lblBnextCar.AutoSize = true;
            this.lblBnextCar.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBnextCar.Location = new System.Drawing.Point(110, 197);
            this.lblBnextCar.Name = "lblBnextCar";
            this.lblBnextCar.Size = new System.Drawing.Size(86, 49);
            this.lblBnextCar.TabIndex = 2;
            this.lblBnextCar.Text = "car";
            // 
            // lblBnextCub
            // 
            this.lblBnextCub.AutoSize = true;
            this.lblBnextCub.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBnextCub.Location = new System.Drawing.Point(8, 102);
            this.lblBnextCub.Name = "lblBnextCub";
            this.lblBnextCub.Size = new System.Drawing.Size(256, 49);
            this.lblBnextCub.TabIndex = 1;
            this.lblBnextCub.Text = "racer name";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(81, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(151, 49);
            this.label9.TabIndex = 0;
            this.label9.Text = "Yellow";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.lblCnextCar);
            this.panel7.Controls.Add(this.lblCnextCub);
            this.panel7.Controls.Add(this.label13);
            this.panel7.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel7.Location = new System.Drawing.Point(718, 505);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(345, 289);
            this.panel7.TabIndex = 8;
            // 
            // lblCnextCar
            // 
            this.lblCnextCar.AutoSize = true;
            this.lblCnextCar.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCnextCar.Location = new System.Drawing.Point(134, 197);
            this.lblCnextCar.Name = "lblCnextCar";
            this.lblCnextCar.Size = new System.Drawing.Size(86, 49);
            this.lblCnextCar.TabIndex = 2;
            this.lblCnextCar.Text = "car";
            // 
            // lblCnextCub
            // 
            this.lblCnextCub.AutoSize = true;
            this.lblCnextCub.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCnextCub.Location = new System.Drawing.Point(4, 102);
            this.lblCnextCub.Name = "lblCnextCub";
            this.lblCnextCub.Size = new System.Drawing.Size(256, 49);
            this.lblCnextCub.TabIndex = 1;
            this.lblCnextCub.Text = "racer name";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(88, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(146, 49);
            this.label13.TabIndex = 0;
            this.label13.Text = "Green";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(88, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 49);
            this.label3.TabIndex = 0;
            this.label3.Text = "Blue";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lblDnextCar);
            this.panel4.Controls.Add(this.lblDnextCub);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel4.Location = new System.Drawing.Point(1069, 505);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(345, 289);
            this.panel4.TabIndex = 9;
            // 
            // lblDnextCar
            // 
            this.lblDnextCar.AutoSize = true;
            this.lblDnextCar.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDnextCar.Location = new System.Drawing.Point(134, 197);
            this.lblDnextCar.Name = "lblDnextCar";
            this.lblDnextCar.Size = new System.Drawing.Size(86, 49);
            this.lblDnextCar.TabIndex = 2;
            this.lblDnextCar.Text = "car";
            // 
            // lblDnextCub
            // 
            this.lblDnextCub.AutoSize = true;
            this.lblDnextCub.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDnextCub.Location = new System.Drawing.Point(4, 102);
            this.lblDnextCub.Name = "lblDnextCub";
            this.lblDnextCub.Size = new System.Drawing.Size(256, 49);
            this.lblDnextCub.TabIndex = 1;
            this.lblDnextCub.Text = "racer name";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel8.Controls.Add(this.lblDtime);
            this.panel8.Controls.Add(this.lblDplace);
            this.panel8.Controls.Add(this.lblDcar);
            this.panel8.Controls.Add(this.lblDcub);
            this.panel8.Controls.Add(this.label15);
            this.panel8.Location = new System.Drawing.Point(1067, 17);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(345, 394);
            this.panel8.TabIndex = 6;
            // 
            // lblDtime
            // 
            this.lblDtime.AutoSize = true;
            this.lblDtime.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDtime.Location = new System.Drawing.Point(123, 331);
            this.lblDtime.Name = "lblDtime";
            this.lblDtime.Size = new System.Drawing.Size(97, 29);
            this.lblDtime.TabIndex = 4;
            this.lblDtime.Text = "0.0000";
            // 
            // lblDplace
            // 
            this.lblDplace.AutoSize = true;
            this.lblDplace.Font = new System.Drawing.Font("Verdana", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDplace.Location = new System.Drawing.Point(134, 226);
            this.lblDplace.Name = "lblDplace";
            this.lblDplace.Size = new System.Drawing.Size(60, 62);
            this.lblDplace.TabIndex = 3;
            this.lblDplace.Text = "1";
            // 
            // lblDcar
            // 
            this.lblDcar.AutoSize = true;
            this.lblDcar.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDcar.Location = new System.Drawing.Point(35, 142);
            this.lblDcar.Name = "lblDcar";
            this.lblDcar.Size = new System.Drawing.Size(99, 59);
            this.lblDcar.TabIndex = 2;
            this.lblDcar.Text = "car";
            // 
            // lblDcub
            // 
            this.lblDcub.AutoSize = true;
            this.lblDcub.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDcub.Location = new System.Drawing.Point(3, 66);
            this.lblDcub.Name = "lblDcub";
            this.lblDcub.Size = new System.Drawing.Size(299, 59);
            this.lblDcub.TabIndex = 1;
            this.lblDcub.Text = "racer name";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Verdana", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(64, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(217, 48);
            this.label15.TabIndex = 0;
            this.label15.Text = "Blue Lane";
            // 
            // FormProjected
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1440, 811);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "FormProjected";
            this.Text = "Heat Results";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.Label lblAnextCar;
        public System.Windows.Forms.Label lblAnextCub;
        public System.Windows.Forms.Label lblBnextCar;
        public System.Windows.Forms.Label lblBnextCub;
        public System.Windows.Forms.Label lblCnextCar;
        public System.Windows.Forms.Label lblCnextCub;
        public System.Windows.Forms.Label lblAtime;
        public System.Windows.Forms.Label lblAplace;
        public System.Windows.Forms.Label lblAcar;
        public System.Windows.Forms.Label lblAcub;
        public System.Windows.Forms.Label lblBtime;
        public System.Windows.Forms.Label lblBplace;
        public System.Windows.Forms.Label lblBcar;
        public System.Windows.Forms.Label lblBcub;
        public System.Windows.Forms.Label lblCtime;
        public System.Windows.Forms.Label lblCplace;
        public System.Windows.Forms.Label lblCcar;
        public System.Windows.Forms.Label lblCcub;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Label lblDnextCar;
        public System.Windows.Forms.Label lblDnextCub;
        private System.Windows.Forms.Panel panel8;
        public System.Windows.Forms.Label lblDtime;
        public System.Windows.Forms.Label lblDplace;
        public System.Windows.Forms.Label lblDcar;
        public System.Windows.Forms.Label lblDcub;
        private System.Windows.Forms.Label label15;
    }
}