﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerbyTimer
{
    class serial
    {
        int blockLimit = 100;
        byte[] buffer = new byte[blockLimit];

        Action kickoffRead = delegate {
	        port.BaseStream.BeginRead(buffer, 0, buffer.Length,delegate (IAsyncResult ar) {
	        try {
	            int actualLength = port.BaseStream.EndRead(ar);
	            byte[] received = new byte[actualLength];
	            Buffer.BlockCopy(buffer, 0, received, 0, actualLength);
	            raiseAppSerialDataEvent(received);
	            }
	        catch (IOException exc) {
	            handleAppSerialError(exc);
	        }
	        kickoffRead();
	    }, null);
	}
	kickoffRead();

    }
}
