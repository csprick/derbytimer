﻿using System;


namespace DerbyTimer
{
    [Serializable()]
    public class car
    {
        

        public String cubName{get; set;}
        public String carName { get; set; }
        public decimal weight { get; set; }
        public int section { get; set; }
        public int[] lane { get; set; }
        public int[] heat { get; set; }
        public int[] place { get; set; }
        public double[] time { get; set; }
        public double totalTime {get; set;}
        public int currentLane { get; set; }
        public int currentHeat { get; set; }
        public double currentTime { get; set; }
        public int currentPlace { get; set; }

        public car()
        {
            lane = new int[6];
            heat = new int[6];
            time = new double[6];
            place = new int[6];
        }
    }
}
