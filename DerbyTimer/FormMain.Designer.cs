﻿namespace DerbyTimer
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.connectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemSimulate = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripComBox = new System.Windows.Forms.ToolStripComboBox();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spectatorDisplayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leaderboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnAddCar = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.cubNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.carNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weightDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sectionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.currentLaneDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.currentHeatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.currentTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.currentPlaceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.carBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnSetupHeat = new System.Windows.Forms.Button();
            this.btnRace = new System.Windows.Forms.Button();
            this.nudRound = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.btnResults = new System.Windows.Forms.Button();
            this.nudHeat = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.btnPlaces = new System.Windows.Forms.Button();
            this.lblLast = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.lblNumHeats = new System.Windows.Forms.Label();
            this.txtLocation = new System.Windows.Forms.TextBox();
            this.dateDate = new System.Windows.Forms.DateTimePicker();
            this.nudLanes = new System.Windows.Forms.NumericUpDown();
            this.nudRace = new System.Windows.Forms.NumericUpDown();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.carBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRound)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHeat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLanes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRace)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.timerToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(9, 3, 0, 3);
            this.menuStrip1.Size = new System.Drawing.Size(1474, 35);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(50, 29);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(195, 30);
            this.openToolStripMenuItem.Text = "Open...";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(195, 30);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(195, 30);
            this.saveAsToolStripMenuItem.Text = "Save as...";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(195, 30);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // timerToolStripMenuItem
            // 
            this.timerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectToolStripMenuItem,
            this.menuItemSimulate,
            this.toolStripSeparator1,
            this.toolStripComBox});
            this.timerToolStripMenuItem.Name = "timerToolStripMenuItem";
            this.timerToolStripMenuItem.Size = new System.Drawing.Size(68, 29);
            this.timerToolStripMenuItem.Text = "Timer";
            // 
            // connectToolStripMenuItem
            // 
            this.connectToolStripMenuItem.CheckOnClick = true;
            this.connectToolStripMenuItem.Name = "connectToolStripMenuItem";
            this.connectToolStripMenuItem.Size = new System.Drawing.Size(194, 30);
            this.connectToolStripMenuItem.Text = "Connect";
            this.connectToolStripMenuItem.Click += new System.EventHandler(this.connectToolStripMenuItem_Click);
            // 
            // menuItemSimulate
            // 
            this.menuItemSimulate.CheckOnClick = true;
            this.menuItemSimulate.Name = "menuItemSimulate";
            this.menuItemSimulate.Size = new System.Drawing.Size(194, 30);
            this.menuItemSimulate.Text = "Simulate";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(191, 6);
            // 
            // toolStripComBox
            // 
            this.toolStripComBox.DropDownHeight = 30;
            this.toolStripComBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComBox.IntegralHeight = false;
            this.toolStripComBox.Name = "toolStripComBox";
            this.toolStripComBox.Size = new System.Drawing.Size(121, 33);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.spectatorDisplayToolStripMenuItem,
            this.leaderboardToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(61, 29);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // spectatorDisplayToolStripMenuItem
            // 
            this.spectatorDisplayToolStripMenuItem.CheckOnClick = true;
            this.spectatorDisplayToolStripMenuItem.Name = "spectatorDisplayToolStripMenuItem";
            this.spectatorDisplayToolStripMenuItem.Size = new System.Drawing.Size(236, 30);
            this.spectatorDisplayToolStripMenuItem.Text = "Spectator Display";
            this.spectatorDisplayToolStripMenuItem.Click += new System.EventHandler(this.spectatorDisplayToolStripMenuItem_Click);
            // 
            // leaderboardToolStripMenuItem
            // 
            this.leaderboardToolStripMenuItem.Enabled = false;
            this.leaderboardToolStripMenuItem.Name = "leaderboardToolStripMenuItem";
            this.leaderboardToolStripMenuItem.Size = new System.Drawing.Size(236, 30);
            this.leaderboardToolStripMenuItem.Text = "Leaderboard";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(61, 29);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(147, 30);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(721, 63);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 25);
            this.label2.TabIndex = 3;
            this.label2.Text = "Lanes:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(373, 64);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Date:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(10, 63);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 25);
            this.label5.TabIndex = 6;
            this.label5.Text = "Location:";
            // 
            // btnAddCar
            // 
            this.btnAddCar.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCar.Location = new System.Drawing.Point(1067, 57);
            this.btnAddCar.Name = "btnAddCar";
            this.btnAddCar.Size = new System.Drawing.Size(149, 35);
            this.btnAddCar.TabIndex = 9;
            this.btnAddCar.Text = "Add Car";
            this.btnAddCar.UseVisualStyleBackColor = true;
            this.btnAddCar.Click += new System.EventHandler(this.btnAddCar_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cubNameDataGridViewTextBoxColumn,
            this.carNameDataGridViewTextBoxColumn,
            this.weightDataGridViewTextBoxColumn,
            this.sectionDataGridViewTextBoxColumn,
            this.totalTimeDataGridViewTextBoxColumn,
            this.currentLaneDataGridViewTextBoxColumn,
            this.currentHeatDataGridViewTextBoxColumn,
            this.currentTimeDataGridViewTextBoxColumn,
            this.currentPlaceDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.carBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(15, 160);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1443, 765);
            this.dataGridView1.TabIndex = 10;
            // 
            // cubNameDataGridViewTextBoxColumn
            // 
            this.cubNameDataGridViewTextBoxColumn.DataPropertyName = "cubName";
            this.cubNameDataGridViewTextBoxColumn.HeaderText = "cubName";
            this.cubNameDataGridViewTextBoxColumn.Name = "cubNameDataGridViewTextBoxColumn";
            // 
            // carNameDataGridViewTextBoxColumn
            // 
            this.carNameDataGridViewTextBoxColumn.DataPropertyName = "carName";
            this.carNameDataGridViewTextBoxColumn.HeaderText = "carName";
            this.carNameDataGridViewTextBoxColumn.Name = "carNameDataGridViewTextBoxColumn";
            // 
            // weightDataGridViewTextBoxColumn
            // 
            this.weightDataGridViewTextBoxColumn.DataPropertyName = "weight";
            this.weightDataGridViewTextBoxColumn.HeaderText = "weight";
            this.weightDataGridViewTextBoxColumn.Name = "weightDataGridViewTextBoxColumn";
            // 
            // sectionDataGridViewTextBoxColumn
            // 
            this.sectionDataGridViewTextBoxColumn.DataPropertyName = "section";
            this.sectionDataGridViewTextBoxColumn.HeaderText = "section";
            this.sectionDataGridViewTextBoxColumn.Name = "sectionDataGridViewTextBoxColumn";
            // 
            // totalTimeDataGridViewTextBoxColumn
            // 
            this.totalTimeDataGridViewTextBoxColumn.DataPropertyName = "totalTime";
            this.totalTimeDataGridViewTextBoxColumn.HeaderText = "totalTime";
            this.totalTimeDataGridViewTextBoxColumn.Name = "totalTimeDataGridViewTextBoxColumn";
            // 
            // currentLaneDataGridViewTextBoxColumn
            // 
            this.currentLaneDataGridViewTextBoxColumn.DataPropertyName = "currentLane";
            this.currentLaneDataGridViewTextBoxColumn.HeaderText = "currentLane";
            this.currentLaneDataGridViewTextBoxColumn.Name = "currentLaneDataGridViewTextBoxColumn";
            // 
            // currentHeatDataGridViewTextBoxColumn
            // 
            this.currentHeatDataGridViewTextBoxColumn.DataPropertyName = "currentHeat";
            this.currentHeatDataGridViewTextBoxColumn.HeaderText = "currentHeat";
            this.currentHeatDataGridViewTextBoxColumn.Name = "currentHeatDataGridViewTextBoxColumn";
            // 
            // currentTimeDataGridViewTextBoxColumn
            // 
            this.currentTimeDataGridViewTextBoxColumn.DataPropertyName = "currentTime";
            this.currentTimeDataGridViewTextBoxColumn.HeaderText = "currentTime";
            this.currentTimeDataGridViewTextBoxColumn.Name = "currentTimeDataGridViewTextBoxColumn";
            // 
            // currentPlaceDataGridViewTextBoxColumn
            // 
            this.currentPlaceDataGridViewTextBoxColumn.DataPropertyName = "currentPlace";
            this.currentPlaceDataGridViewTextBoxColumn.HeaderText = "currentPlace";
            this.currentPlaceDataGridViewTextBoxColumn.Name = "currentPlaceDataGridViewTextBoxColumn";
            // 
            // carBindingSource
            // 
            this.carBindingSource.DataSource = typeof(DerbyTimer.car);
            // 
            // btnSetupHeat
            // 
            this.btnSetupHeat.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSetupHeat.Location = new System.Drawing.Point(172, 109);
            this.btnSetupHeat.Name = "btnSetupHeat";
            this.btnSetupHeat.Size = new System.Drawing.Size(162, 42);
            this.btnSetupHeat.TabIndex = 11;
            this.btnSetupHeat.Text = "Setup Round";
            this.btnSetupHeat.UseVisualStyleBackColor = true;
            this.btnSetupHeat.Click += new System.EventHandler(this.btnSetupHeat_Click);
            // 
            // btnRace
            // 
            this.btnRace.Enabled = false;
            this.btnRace.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRace.Location = new System.Drawing.Point(772, 109);
            this.btnRace.Name = "btnRace";
            this.btnRace.Size = new System.Drawing.Size(162, 42);
            this.btnRace.TabIndex = 12;
            this.btnRace.Text = "Race!";
            this.btnRace.UseVisualStyleBackColor = true;
            this.btnRace.Click += new System.EventHandler(this.btnRace_Click);
            this.btnRace.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnRaceMouseDown);
            // 
            // nudRound
            // 
            this.nudRound.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudRound.Location = new System.Drawing.Point(105, 111);
            this.nudRound.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.nudRound.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudRound.Name = "nudRound";
            this.nudRound.Size = new System.Drawing.Size(60, 37);
            this.nudRound.TabIndex = 13;
            this.nudRound.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 29);
            this.label3.TabIndex = 14;
            this.label3.Text = "Round:";
            // 
            // btnResults
            // 
            this.btnResults.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResults.Location = new System.Drawing.Point(1000, 109);
            this.btnResults.Name = "btnResults";
            this.btnResults.Size = new System.Drawing.Size(162, 42);
            this.btnResults.TabIndex = 16;
            this.btnResults.Text = "Results";
            this.btnResults.UseVisualStyleBackColor = true;
            this.btnResults.Click += new System.EventHandler(this.btnResults_Click);
            // 
            // nudHeat
            // 
            this.nudHeat.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudHeat.Location = new System.Drawing.Point(449, 113);
            this.nudHeat.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nudHeat.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudHeat.Name = "nudHeat";
            this.nudHeat.Size = new System.Drawing.Size(62, 37);
            this.nudHeat.TabIndex = 17;
            this.nudHeat.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(373, 115);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 29);
            this.label4.TabIndex = 18;
            this.label4.Text = "Heat";
            // 
            // btnPlaces
            // 
            this.btnPlaces.Enabled = false;
            this.btnPlaces.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlaces.Location = new System.Drawing.Point(603, 109);
            this.btnPlaces.Name = "btnPlaces";
            this.btnPlaces.Size = new System.Drawing.Size(162, 42);
            this.btnPlaces.TabIndex = 19;
            this.btnPlaces.Text = "Places";
            this.btnPlaces.UseVisualStyleBackColor = true;
            this.btnPlaces.Click += new System.EventHandler(this.btnPlaces_Click);
            // 
            // lblLast
            // 
            this.lblLast.AutoSize = true;
            this.lblLast.Location = new System.Drawing.Point(1328, 58);
            this.lblLast.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLast.Name = "lblLast";
            this.lblLast.Size = new System.Drawing.Size(105, 20);
            this.lblLast.TabIndex = 20;
            this.lblLast.Text = "last: R 0 - H 0";
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Location = new System.Drawing.Point(1255, 110);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(178, 41);
            this.btnClear.TabIndex = 21;
            this.btnClear.Text = "Clear Results";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(873, 63);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 25);
            this.label6.TabIndex = 25;
            this.label6.Text = "Race:";
            // 
            // lblNumHeats
            // 
            this.lblNumHeats.AutoSize = true;
            this.lblNumHeats.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumHeats.Location = new System.Drawing.Point(524, 115);
            this.lblNumHeats.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNumHeats.Name = "lblNumHeats";
            this.lblNumHeats.Size = new System.Drawing.Size(60, 29);
            this.lblNumHeats.TabIndex = 27;
            this.lblNumHeats.Text = "of X";
            // 
            // txtLocation
            // 
            this.txtLocation.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.Location = new System.Drawing.Point(119, 58);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Size = new System.Drawing.Size(254, 34);
            this.txtLocation.TabIndex = 29;
            // 
            // dateDate
            // 
            this.dateDate.CustomFormat = "ddd dd MMM yy";
            this.dateDate.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateDate.Location = new System.Drawing.Point(453, 57);
            this.dateDate.Name = "dateDate";
            this.dateDate.Size = new System.Drawing.Size(247, 34);
            this.dateDate.TabIndex = 30;
            // 
            // nudLanes
            // 
            this.nudLanes.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudLanes.Location = new System.Drawing.Point(801, 58);
            this.nudLanes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nudLanes.Name = "nudLanes";
            this.nudLanes.Size = new System.Drawing.Size(62, 37);
            this.nudLanes.TabIndex = 31;
            this.nudLanes.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.nudLanes.ValueChanged += new System.EventHandler(this.nudLanes_ValueChanged);
            // 
            // nudRace
            // 
            this.nudRace.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudRace.Location = new System.Drawing.Point(951, 57);
            this.nudRace.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nudRace.Name = "nudRace";
            this.nudRace.Size = new System.Drawing.Size(62, 37);
            this.nudRace.TabIndex = 32;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1474, 942);
            this.Controls.Add(this.nudRace);
            this.Controls.Add(this.nudLanes);
            this.Controls.Add(this.dateDate);
            this.Controls.Add(this.txtLocation);
            this.Controls.Add(this.lblNumHeats);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.lblLast);
            this.Controls.Add(this.btnPlaces);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.nudHeat);
            this.Controls.Add(this.btnResults);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nudRound);
            this.Controls.Add(this.btnRace);
            this.Controls.Add(this.btnSetupHeat);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnAddCar);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FormMain";
            this.Text = "Derby Timer";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.carBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRound)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHeat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLanes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRace)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem timerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem connectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leaderboardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnAddCar;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnSetupHeat;
        private System.Windows.Forms.Button btnRace;
        private System.Windows.Forms.ToolStripMenuItem menuItemSimulate;
        private System.Windows.Forms.NumericUpDown nudRound;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.BindingSource carBindingSource;
        private System.Windows.Forms.Button btnResults;
        private System.Windows.Forms.NumericUpDown nudHeat;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnPlaces;
        private System.Windows.Forms.Label lblLast;
        public System.Windows.Forms.ToolStripMenuItem spectatorDisplayToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn cubNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn carNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn weightDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sectionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn currentLaneDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn currentHeatDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn currentTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn currentPlaceDataGridViewTextBoxColumn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripComboBox toolStripComBox;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblNumHeats;
        private System.Windows.Forms.TextBox txtLocation;
        private System.Windows.Forms.DateTimePicker dateDate;
        private System.Windows.Forms.NumericUpDown nudLanes;
        private System.Windows.Forms.NumericUpDown nudRace;
    }
}

