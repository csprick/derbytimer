﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;
using System.IO.Ports;

using System.Diagnostics;

namespace DerbyTimer
{
    public partial class FormMain : Form
    {
        enum lanes { A = 1, B, C, D, E, F, G };
        int numLanes=4;
        int numHeats;
        int selectedRound;
        int thisHeat;
        bool racing = false;

        BindingSource source = new BindingSource();
        List<car> cars = new List<car>();
        car[] currentHeat = new car[5];
        car[] nextHeat = new car[5];
        car nullCar = new car();
        
        FormProjected remote = new FormProjected();
        FormResults frmResults = new FormResults();

        XmlSerializer ser = new XmlSerializer(typeof(car));
        String filepath;
        

        SerialPort comPort = new SerialPort("COM4", 9600, Parity.None, 8, StopBits.One);

        public FormMain()
        {
            InitializeComponent();
            //comboLanes.SelectedItem = 3;    
            
            source.DataSource = cars;
            dataGridView1.DataSource = source;

            remote.Show();
            // Attach a method to be called when there
            // is data waiting in the port's buffer
            //com3.DataReceived += new SerialDataReceivedEventHandler(com3_DataReceived);
            this.Focus();
            string[] ports = SerialPort.GetPortNames();
            toolStripComBox.Items.AddRange(ports);
            nullCar.carName = "---";
            nullCar.cubName = "---";
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Display frmAbout as a modal dialog
            FormAbout formAbout = new FormAbout();
            formAbout.ShowDialog();
            // to show another form but not as a dialog: form2.Show();
        }

        private void spectatorDisplayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (spectatorDisplayToolStripMenuItem.Checked)
                remote.Hide();
            else
                remote.Show();
        }

        private void btnAddCar_Click(object sender, EventArgs e)
        {
            using (FormCarDetails carDetails = new FormCarDetails())
            {
                var result = carDetails.ShowDialog();
                if (result == DialogResult.OK)
                {
                    car newCar = new car();
                    newCar.carName = carDetails.CarName.Text;
                    newCar.cubName = carDetails.CubName.Text;
                    newCar.weight = carDetails.weight.Value;
                    newCar.section = carDetails.section.SelectedIndex;

                    cars.Add(newCar);
                    source.ResetBindings(false);
                }
                else if (result == DialogResult.Yes)
                {
                    car newCar = new car();
                    newCar.carName = carDetails.CarName.Text;
                    newCar.cubName = carDetails.CubName.Text;
                    newCar.weight = carDetails.weight.Value;
                    newCar.section = carDetails.section.SelectedIndex;

                    cars.Add(newCar);
                    source.ResetBindings(false);
                    btnAddCar_Click(this, e);
                }

            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                filepath = saveFileDialog1.FileName;
                SaveFile();
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (filepath == null)
                saveAsToolStripMenuItem_Click(this, null);
            else
                SaveFile();
        }

        private void SaveFile()
        {
            try
            {
                using (var writer = new StreamWriter(filepath))
                {
                    writer.WriteLine("{0},{1},{2},{3}",txtLocation.Text, dateDate.Text,nudLanes.Text,nudRace.Text);
                    
                    //Write column headers
                    writer.Write("Cub Name,Car Name,Weight,Section");
                    writer.Write(",TotalTime");
                    for (int i=1; i < numLanes+1; i++)
                        writer.Write(",Heat {0},Lane {0},Time {0},Place {0}", i);
                    writer.WriteLine();

                    //Write data
                    foreach (car c in cars)
                    {
                        writer.Write("{0},{1},{2},{3}", c.cubName, c.carName, c.weight, c.section);
                        writer.Write(",{0}", c.totalTime); //finish record
                        for (int i = 0; i < numLanes; i++)
                        {
                            writer.Write(",{0},{1},{2},{3}", c.heat[i], c.lane[i], c.time[i], c.place[i]);
                        }
                        writer.WriteLine();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int raceNum;
           String line;
           OpenFileDialog openFileDialog1 = new OpenFileDialog();
           cars.Clear();

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    cars.Clear();
                    using (var reader = new StreamReader(openFileDialog1.FileName))
                    {
                        // First line is the race info (location, date, num lanes
                        line = reader.ReadLine();
                        string[] header = line.Split(',');
                        txtLocation.Text = header[0];
                        dateDate.Text = header[1];
                        nudLanes.Text = header[2];
                        numLanes = Convert.ToInt16(header[2]);
                        nudRace.Text = header[3];
                        raceNum = Convert.ToInt16(header[3]);

                        //Second line - discard, it is the column headers for excel
                        line = reader.ReadLine(); 

                        //The rest of the lines are racers
                        while ((line = reader.ReadLine()) != null)
                        {
                            string[] parts = line.Split(',');
                            //int heats = (parts.Length-4)/4;
                            //assign to proper member of cars
                            car newCar = new car();
                            
                            newCar.cubName = parts[0];
                            newCar.carName = parts[1];
                            newCar.weight = Convert.ToInt16(parts[2]);
                            newCar.section = Convert.ToInt16(parts[3]);
                            if (raceNum > 0)
                            {
                                newCar.totalTime = Convert.ToDouble(parts[4]);
                                for (int i = 0; i < numLanes; i++)
                                {
                                    newCar.heat[i] = Convert.ToInt16(parts[5 + i * 4]);
                                    newCar.lane[i] = Convert.ToInt16(parts[6 + i * 4]);
                                    newCar.time[i] = Convert.ToDouble(parts[7 + i * 4]);
                                    newCar.place[i] = Convert.ToInt16(parts[8 + 1 * 4]);
                                }
                            }
                            cars.Add(newCar);
                        }
                    }
                    source.ResetBindings(false);
                    filepath = openFileDialog1.FileName;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private string SimulateTimes (int round)
        {
            Random r = new Random();
            string results;
            double[] times = new double[6];

            //Generate simulated times for each lane
            for (int i=0; i<6; i++)
                times[i] = r.NextDouble()*(4-2)+2;

            Array.Sort(times);

            results = "@A=" + times[0]
                    + "! B=" + times[1]
                    + "\" C=" + times[2]
                    + "# D=" + times[3] 
                    + "$ E=" + times[4]
                    + "% F=" + times[5];
//Partial result test code. A & B only
/*            results = "@A=" + times[0]
                + "! B=" + times[1]
                + "\" C=0.000"
                + "  D=0.000"
                + "  E=0.000"
                + "  F=0.000";*/
            return results;
            
        }

        private void SumTimes()
        {
            foreach (car c in cars)
            {
                c.totalTime = 0;
                for (int i = 0; i < numLanes; i++)
                    c.totalTime = c.totalTime + c.time[i];
            }
            source.ResetBindings(false);
        }

        private void UpdateCurrent(int round)
        {
            foreach (car c in cars)
            {
                c.currentHeat = c.heat[round];
                c.currentLane = c.lane[round];
                c.currentTime = c.time[round];
                c.currentPlace = c.place[round];
                
            }
            source.ResetBindings(false);
        }

        private void UpdateData(int round)
        {
            foreach (car c in cars)
            {
                c.heat[round] = c.currentHeat;
                c.lane[round] = c.currentLane;
                c.time[round] = c.currentTime;
                c.place[round] = c.currentPlace;
            }

        }

        private void btnSetupHeat_Click(object sender, EventArgs e)
        {
            int heat = 1;
            
            int selectedRound = (int)nudRound.Value-1;
            //Update 'current' values for selected heat
            UpdateCurrent(selectedRound);

            //Sort cars by totalTime 
            if (selectedRound != 0)
                cars.Sort((x,y) => x.totalTime.CompareTo(y.totalTime));

            
            int lane = 0;
            if (selectedRound == 0)
            {
                //Assign lanes
                foreach (car c in cars)
                {
                    lane++;
                    c.currentLane = lane;
                    if (lane == numLanes)
                        lane = 0;
                }
            }
            else
            {
                //increment lanes
                foreach (car c in cars)
                {
                    if (c.lane[selectedRound-1] == numLanes)
                        c.currentLane = 1;
                    else
                        c.currentLane = c.lane[selectedRound-1] + 1;
                }
            }
            foreach (car c in cars)
                c.currentHeat = 99;

            source.ResetBindings(false);
            
            while (heat >=1)
            {
                heat = AssignHeats(heat);
                //resort
                //cars.Sort((x, y) => x.totalTime.CompareTo(y.totalTime));
                //cars.Sort((x, y) => x.currentHeat.CompareTo(y.currentHeat));
            }

            UpdateData(selectedRound);
            int extraHeat = 0;
           
            if ((cars.Count() % numLanes) > 0)
                extraHeat = 1;
            else extraHeat = 0;
            numHeats = (cars.Count()/numLanes) + extraHeat;
            lblNumHeats.Text = "of " + numHeats;
            nudHeat.Value = 1;

            btnPlaces.Enabled = true;
            btnRace.Enabled = false;

            //Sort display by heat
            cars.Sort((x, y) => x.currentLane.CompareTo(y.currentLane));
            cars.Sort((x, y) => x.currentHeat.CompareTo(y.currentHeat));
            source.ResetBindings(false);
        }

        private bool LaneAvailable(byte lanes, int l)
        {
            //given a lane to test 'l', check if that lane is marked used in 'lanes'
            //    e.g. if lanes = 011 and we are checking lane 2
            //         011 & 2 = 2  which means that lane has been used
            switch (l) 
            {
                case 1:
                    if ((lanes & 1) == 1)
                        return false;
                    break;
                case 2:
                    if ((lanes & 2) == 2)
                        return false;
                    break;
                case 3:
                    if ((lanes & 4) == 4)
                        return false;
                    break;
                case 4:
                    if ((lanes & 8) == 8)
                        return false;
                    break;
                case 5:
                    if ((lanes & 16) == 16)
                        return false;
                    break;
            }
            return true;            
        }

        private byte UseLane(byte lanes, int l)
        {
            switch (l)
            {
                case 1:
                    return lanes |= 1;
                case 2:
                    return lanes |= 2;
                case 3:
                    return lanes |= 4;
                case 4:
                    return lanes |= 8;
                case 5:
                    return lanes |= 16;
            }
            return 0;
        }

        private int AssignHeats(int heat)
        {
            //Assign heats/race groups
            
            byte lanes=0; //bitfield indicating lane useage for current heat
            int allLanes = 0;

            switch (numLanes)
            {
                case 1:
                    allLanes = 1;
                    break;
                case 2:
                    allLanes = 3;
                    break;
                case 3:
                    allLanes = 7; // 7 = 111 = 3 lanes used
                    break;
                case 4:
                    allLanes = 15; //15 = 1111 = 4 lanes used
                    break;
                case 5:
                    allLanes = 31;
                    break;
            }
            foreach (car c in cars)
            {
                if (c.currentHeat >= heat)
                {
                    if (LaneAvailable(lanes, c.currentLane))
                    {
                        c.currentHeat = heat;
                        lanes = UseLane(lanes, c.currentLane);
                    }
                    else  //lane was used, so ... push this car to the next heat
                    {
                        c.currentHeat = heat + 1;
                    }

                    if (lanes == allLanes)
                    {
                        heat++;  // next heat
                        return heat;
                    }
                }
            }
            return -1;
        }

        private void btnPlaces_Click(object sender, EventArgs e)
        {
            btnSetupHeat.Enabled = false;
            selectedRound = (int)nudRound.Value - 1;
            thisHeat = (int)nudHeat.Value;

            //reset labels
            for (int x=1; x<numLanes+1; x++)
            {
                currentHeat[x] = nullCar;
                nextHeat[x] = nullCar;
            }
            //get groups of cars to race 
            foreach (car c in cars)
            {
                if (c.currentHeat == thisHeat)
                    currentHeat[c.currentLane] = c;
                if (c.currentHeat == thisHeat + 1)
                    nextHeat[c.currentLane] = c;
            }

            btnPlaces.Enabled = false;
            btnRace.Enabled = true;
            //Display current and next races on projected window
            PopulateRemote();
        }

        private bool IsRA(string s)
        {
            //Variations observed of returned string from command "RA". Result data may or may not follow.
            switch (s)
            {
                case "RA":
                case "@RA":
                case "RA*":
                case "@RA*":
                    return true;
            }
            return false;
        }

        private void btnRace_Click(object sender, EventArgs e)
        {
            string results;

            if (racing)
            {
                racing = false;
                return;
            }
            racing = true;
            btnRace.Text = "End Race";

            if (menuItemSimulate.Checked)
                results = SimulateTimes(selectedRound);
            else
            {
                //get times
                // Note: ReadLine is a blocking call and won't return until all lanes have finished
                //       Should consider using Read instead and looking for EOL to process results.
                //              results = comPort.ReadLine();
                try
                {
                    comPort.DiscardInBuffer();  //discard any remaining data (there seems to be several spaces after \r\n and an @)
                }
                catch (InvalidOperationException)
                {
                    MessageBox.Show("Select and COM port from the Timer menu and Connect");
                    racing = false;
                    btnRace.BackColor = Control.DefaultBackColor;
                    btnRace.Text = "Race!";
                    return;
                }
                char[] buffer = new char[64];     //normally ready 55 char's
                int offset = 0;
                while (offset < buffer.Count())
                {
                    try
                    {
                        //Read 1 byte
                        if (comPort.Read(buffer, offset, 1) > 0)
                        {
                            //if cr or lf end read
                            if (buffer[offset] == '\r' || buffer[offset] == '\n')
                            {
                                //Check if the RA command was sent, if so continue reading
                                if (IsRA(new string(buffer, 0, offset)))
                                    continue;
                                comPort.DiscardInBuffer();  //discard any remaining data (there seems to be several spaces after \r\n and an @)
                                break;
                            }
                            offset++;
                        }
                    }
                    catch (TimeoutException)
                    {
                        //Ignore time-out exception
                        if (IsRA(new string(buffer, 0, offset)))    //If RA was sent and no more data then end read.
                            break;
                    }
                    Application.DoEvents();
                    if (!racing)
                    {
                        //If race button clicked again then check if race should be cancelled.
                        switch (MessageBox.Show("Yes\tEnd this race and collect results\nNo\tContinue this race\nCancel\tEnd this race and discard results", "End Race", MessageBoxButtons.YesNoCancel))
                        {
                            case DialogResult.Yes:
                                comPort.Write("RA");    //RA - Reset lane – Force results
                                break;
                            case DialogResult.Cancel:
                                btnRace.BackColor = Control.DefaultBackColor;
                                btnRace.Text = "Race!";
                                return;
                        }
                        racing = true;
                    }
                }
                results = new string(buffer, 0, offset);
            }
            Debug.WriteLine(results);
            racing = false;
            if (results == "Copyright (C) 2004 Micro Wizard")   //response when finish initialises. I know, but it did happen once.
            {
                btnRace.Enabled = false;
                btnPlaces.Enabled = true;
                btnRace.BackColor = Control.DefaultBackColor;
                btnRace.Text = "Race!";
                return;
            }
            if (results.IndexOf("RA") == 0)        //Force end and results
            {
                results = results.Substring(2);
                if (results == "*")  //No results
                {
                    btnRace.BackColor = Control.DefaultBackColor;
                    btnRace.Text = "Race!";
                    MessageBox.Show("No Results");
                    return;
                }
            }
            else if (results.IndexOf("@RA") == 0)        //Force end and results
            {
                results = results.Substring(3);
                if (results == "*")  //No results
                {
                    btnRace.BackColor = Control.DefaultBackColor;
                    btnRace.Text = "Race!";
                    MessageBox.Show("No Results");
                    return;
                }
            }

            ParseResults(results); //and publish to remote

            //save results back in cars
            for (int i = 1; i < numLanes+1; i++)
            {
                foreach (car c in cars)
                {
                    if (c.cubName == currentHeat[i].cubName)
                    {
                        c.currentPlace = currentHeat[i].currentPlace;
                        c.currentTime = currentHeat[i].currentTime;
                        break;
                    }
                }
            }
            
            UpdateData(selectedRound);
            SumTimes();
            lblLast.Text = "Last: R " + (selectedRound+1) + " H " + thisHeat;
            btnRace.Enabled = false;
            btnPlaces.Enabled = true;
            nudHeat.Value++;

            source.ResetBindings(false);
            btnRace.BackColor = Control.DefaultBackColor;
            btnRace.Text = "Race!";

            if (nudHeat.Value > numHeats)
            {
                nudHeat.Value = 1;
                nudRound.Value++;
                btnSetupHeat.Enabled = true;
                btnPlaces.Enabled = false;
            }
            if (nudRound.Value > numLanes)
                MessageBox.Show("The race is complete!  Check your results.", "Race Done", MessageBoxButtons.OK);
        }

        private void ParseResults(string results)
        {
            // the serial results string looks like this:
            // @A=1.234! B=2.345" C=3.456# D=4.567$ E=5.678% F=6.789&
            // with partial results like this:
            // A=1.501! B=0.000  C=0.000  D=0.000  E=0.000  F=0.000  

            char[] symbols = { '=', '!', '"', '#', '$' };
            string[] localTimes;
            //break string up into parts and assign to currentHeat[]
            // @A=2.088# B=1.952" C=1.737! D=0.000  E=0.000  F=0.000
            // Split on spaces
            string[] localLanes = results.Split(' ').Where(val => val != "").ToArray(); //strip out any empty lines from partial result.

            // split each lane on =
            // record value into currentHeat[]
            for (int x = 0; x < numLanes; x++)
            {
                if (localLanes[x].EndsWith("!"))
                    currentHeat[x+1].currentPlace = 1;
                else if (localLanes[x].EndsWith("\""))
                    currentHeat[x+1].currentPlace = 2;
                else if (localLanes[x].EndsWith("#"))
                    currentHeat[x+1].currentPlace = 3;
                else if (localLanes[x].EndsWith("$"))
                    currentHeat[x+1].currentPlace = 4;
            }

            for (int x = 0; x < numLanes; x++)
            {
                localTimes = localLanes[x].Split(symbols);
                currentHeat[x+1].currentTime = Convert.ToDouble(localTimes[1]);
            }

            //Publish to remote labels
            remote.lblDtime.Text = currentHeat[1].currentTime.ToString();
            remote.lblCtime.Text = currentHeat[2].currentTime.ToString();
            remote.lblBtime.Text = currentHeat[3].currentTime.ToString();
            remote.lblAtime.Text = currentHeat[4].currentTime.ToString();

            remote.lblDplace.Text = currentHeat[1].currentPlace.ToString();
            remote.lblCplace.Text = currentHeat[2].currentPlace.ToString();
            remote.lblBplace.Text = currentHeat[3].currentPlace.ToString();
            remote.lblAplace.Text = currentHeat[4].currentPlace.ToString();
        }

        private void PopulateRemote()
        {
            remote.lblDcar.Text = currentHeat[1].carName;
            remote.lblDcub.Text = currentHeat[1].cubName;
            remote.lblCcar.Text = currentHeat[2].carName;
            remote.lblCcub.Text = currentHeat[2].cubName;
            remote.lblBcar.Text = currentHeat[3].carName;
            remote.lblBcub.Text = currentHeat[3].cubName;
            remote.lblAcub.Text = currentHeat[4].cubName;
            remote.lblAcar.Text = currentHeat[4].carName;

            remote.lblDtime.Text = "---";
            remote.lblDplace.Text = "---";
            remote.lblCtime.Text = "---";
            remote.lblCplace.Text = "---";
            remote.lblBtime.Text = "---";
            remote.lblBplace.Text = "---";
            remote.lblAtime.Text = "---";
            remote.lblAplace.Text = "---";

            remote.lblDnextCar.Text = nextHeat[1].carName;
            remote.lblDnextCub.Text = nextHeat[1].cubName;
            remote.lblCnextCar.Text = nextHeat[2].carName;
            remote.lblCnextCub.Text = nextHeat[2].cubName;
            remote.lblBnextCar.Text = nextHeat[3].carName;
            remote.lblBnextCub.Text = nextHeat[3].cubName;
            remote.lblAnextCar.Text = nextHeat[4].carName;
            remote.lblAnextCub.Text = nextHeat[4].cubName;
        }

        private void btnResults_Click(object sender, EventArgs e)
        {
            //Clear form
            frmResults.listFastestTotals.Items.Clear();
            frmResults.listSlowestTotals.Items.Clear();
            frmResults.listFastestRace.Items.Clear();
            frmResults.listMostWins.Items.Clear();

            //Find top 4 fastest totals
            cars.Sort((x, y) => x.totalTime.CompareTo(y.totalTime));
            int car_count = 4;
            if (cars.Count() < car_count)
                car_count = cars.Count();
            for (int x = 0; x < car_count; x++)
            {
                frmResults.listFastestTotals.Items.Add(cars[x].cubName+" "+cars[x].carName);
            }
            //Find slowest total
            if (car_count > 0)
                frmResults.listSlowestTotals.Items.Add(cars[cars.Count() - 1].cubName + " " + cars[cars.Count() - 1].carName);

            //Find fastest individual time

            //Find top 3 number of wins

            frmResults.Show();
        }

        private void connectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (connectToolStripMenuItem.Checked)
            {
                comPort.PortName = toolStripComBox.SelectedItem.ToString(); // "Com4";
                comPort.Open();
                comPort.ReadTimeout = 100;    //set read time-out to 100 millisecond for non-blocking read.
            }
            else
                comPort.Close();

        }

        //private void com3_DataReceived(object sender, SerialDataReceivedEventArgs e)
        //{
        //    /// <summary> 
        ///// Holds data received until we get a terminator. 
        ///// </summary> 
        // string tString = string.Empty; 
        ///// <summary> 
        ///// End of transmition byte in this case EOT (ASCII 4). 
        ///// </summary> 
        // byte _terminator = 0x4; 

        //    // Show all the incoming data in the port's buffer
        //    Console.WriteLine(com3.ReadExisting()); //This is probably unreliable

        //    //Initialize a buffer to hold the received data 
        //    byte[] buffer = new byte[com3.ReadBufferSize];

        //    //There is no accurate method for checking how many bytes are read 
        //    //unless you check the return from the Read method 
        //    int bytesRead = com3.Read(buffer, 0, buffer.Length);

        //    //For the example assume the data we are received is ASCII data. 
        //    tString += Encoding.ASCII.GetString(buffer, 0, bytesRead);
        //    //Check if string contains the terminator  
        //    if (tString.IndexOf((char)_terminator) > -1)
        //    {
        //        //If tString does contain terminator we cannot assume that it is the last character received 
        //        string workingString = tString.Substring(0, tString.IndexOf((char)_terminator));
        //        //Remove the data up to the terminator from tString 
        //        tString = tString.Substring(tString.IndexOf((char)_terminator));
        //        //Do something with workingString 
        //        Console.WriteLine(workingString);
        //    } 
        //}

        private void btnRaceMouseDown(object sender, MouseEventArgs e)
        {
            btnRace.BackColor = Color.Green;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            // Clear out race results to start again
            if (MessageBox.Show("Are you sure you want to clear results?", "Warning!", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                for (int i = 1; i < numLanes + 1; i++)
                {
                    foreach (car c in cars)
                    {
                        c.currentHeat = 0;
                        c.currentLane = 0;
                        c.currentPlace = 0;
                        c.currentTime = 0;
                        Array.Clear(c.heat, 0, c.heat.Length);
                        Array.Clear(c.lane, 0, c.lane.Length);
                        Array.Clear(c.place, 0, c.place.Length);
                        Array.Clear(c.time, 0, c.time.Length);
                    }
                }
                btnRace.Enabled = false;
                btnPlaces.Enabled = false;
                btnSetupHeat.Enabled = true;
                btnRace.BackColor = Control.DefaultBackColor;
                btnRace.Text = "Race!";
                SumTimes();
                source.ResetBindings(false);
            }
        }

        private void nudLanes_ValueChanged(object sender, EventArgs e)
        {
            numLanes = (int)nudLanes.Value;
        }
    }
}
