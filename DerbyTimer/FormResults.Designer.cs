﻿namespace DerbyTimer
{
    partial class FormResults
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.listFastestTotals = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.listFastestRace = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.listSlowestTotals = new System.Windows.Forms.ListBox();
            this.listMostWins = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Fastest Totals";
            // 
            // listFastestTotals
            // 
            this.listFastestTotals.FormattingEnabled = true;
            this.listFastestTotals.ItemHeight = 20;
            this.listFastestTotals.Location = new System.Drawing.Point(18, 56);
            this.listFastestTotals.Name = "listFastestTotals";
            this.listFastestTotals.Size = new System.Drawing.Size(458, 164);
            this.listFastestTotals.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 248);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(150, 26);
            this.label2.TabIndex = 2;
            this.label2.Text = "Fastest Race";
            // 
            // listFastestRace
            // 
            this.listFastestRace.FormattingEnabled = true;
            this.listFastestRace.ItemHeight = 20;
            this.listFastestRace.Location = new System.Drawing.Point(18, 287);
            this.listFastestRace.Name = "listFastestRace";
            this.listFastestRace.Size = new System.Drawing.Size(458, 164);
            this.listFastestRace.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(521, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(158, 26);
            this.label3.TabIndex = 4;
            this.label3.Text = "Slowest Total";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(521, 248);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 26);
            this.label4.TabIndex = 5;
            this.label4.Text = "Most Wins";
            // 
            // listSlowestTotals
            // 
            this.listSlowestTotals.FormattingEnabled = true;
            this.listSlowestTotals.ItemHeight = 20;
            this.listSlowestTotals.Location = new System.Drawing.Point(505, 56);
            this.listSlowestTotals.Name = "listSlowestTotals";
            this.listSlowestTotals.Size = new System.Drawing.Size(458, 164);
            this.listSlowestTotals.TabIndex = 6;
            // 
            // listMostWins
            // 
            this.listMostWins.FormattingEnabled = true;
            this.listMostWins.ItemHeight = 20;
            this.listMostWins.Location = new System.Drawing.Point(505, 287);
            this.listMostWins.Name = "listMostWins";
            this.listMostWins.Size = new System.Drawing.Size(458, 164);
            this.listMostWins.TabIndex = 7;
            // 
            // FormResults
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(975, 715);
            this.Controls.Add(this.listMostWins);
            this.Controls.Add(this.listSlowestTotals);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.listFastestRace);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listFastestTotals);
            this.Controls.Add(this.label1);
            this.Name = "FormResults";
            this.Text = "FormResults";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormResults_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.ListBox listSlowestTotals;
        public System.Windows.Forms.ListBox listMostWins;
        public System.Windows.Forms.ListBox listFastestTotals;
        public System.Windows.Forms.ListBox listFastestRace;
    }
}