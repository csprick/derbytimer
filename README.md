# README #

DerbyTimer is an application for recording and managing Pinewood Derby races using a Microwizard K1 timer.
version 0.1 was strictly for a 3-lane track and is not captured here.
Version 0.2 was created in 2016 for our new 4-lane track.  It is usable with different numbers of lanes.

### How do I get set up? ###

* Summary of set up
This c# program is targeted for .Net 3.5 so it should work on windows 7 and 10.
You'll also need a USB or serial port attached to a Microwizard timer.  Default is 4 lanes.  I haven't tried other than 3 or 4, but it should be generic.
A normally open microswitch is attached to the starting gate and the computer is attached to the finish (timer) by a serial cable.  A usb-serial converter is fine as long as your computer has drivers for it.  The switch is closed (actuated) when the starting gate is in the standby mode.  The switch opens as the cars are released to race.  This switch also resets the timer, so should NOT be closed until the previous race is complete.

* Configuration
You can generate simulated times for testing by selecting 'simulate' from the 'timer' menu.  
For actual racing, select your com port from the dropdown and then click 'connect' to open a serial connection to the timer.

Race configuration (location, date, lanes, race number) as well as Racer and car info and all of the times, lanes, places from the race can be saved and loaded as a .csv file.  This file can be analyzed in Excel as much as you want.  Try times/lane or times/weight for some interesting graphs.  This can also be used to pre-populate racer info to make for quicker setup on race day. 

### Who do I talk to? ###

* Contact Cyle Sprick (Hills to Coast District - South Australian Scouts)
* non-blocking serial reads and other error checking contributed by Peter Fitzsimons